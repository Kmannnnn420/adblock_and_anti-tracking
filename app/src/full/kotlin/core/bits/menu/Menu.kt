/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu

import core.AndroidKontext
import core.LabelVB
import core.bits.menu.adblocking.createAdblockingMenuItem
import core.bits.menu.advanced.createAdvancedMenuItem
import core.bits.menu.apps.createAppsMenuItem
import core.bits.menu.dns.createDnsMenuItem
import core.bits.menu.vpn.createVpnMenuItem
import core.res
import org.blokada.R

fun createMenu(ktx: AndroidKontext): MenuItemsVB {
    return MenuItemsVB(ktx,
            items = listOf(
                    LabelVB(ktx, label = R.string.menu_configure.res()),
                    //createAdblockingMenuItem(ktx),
                    createDnsMenuItem(ktx),
                    //createVpnMenuItem(ktx),
                    LabelVB(ktx, label = R.string.menu_exclude.res()),
                    createAppsMenuItem(ktx),
                    LabelVB(ktx, label = R.string.menu_dive_in.res()),
                    //createDonateMenuItem(ktx),
                    createAdvancedMenuItem(ktx)//,
                    //createLearnMoreMenuItem(ktx),
                    //createAboutMenuItem(ktx)
            ),
            name = R.string.panel_section_menu.res()
    )
}

