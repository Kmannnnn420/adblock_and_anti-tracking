/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu.advanced

import android.widget.Toast
import core.*
import core.Register.set
import core.bits.*
import core.bits.menu.MenuItemsVB
import gs.presentation.NamedViewBinder
import org.blokada.R

fun createMenuAdvanced(ktx: AndroidKontext): NamedViewBinder {
    var items = listOf(
            LabelVB(ktx, label = R.string.label_basic.res()),
            NotificationsVB(ktx, onTap = defaultOnTap),
            StartOnBootVB(ktx, onTap = defaultOnTap),
            LabelVB(ktx, label = R.string.label_advanced.res(), hiddenAction = {
                set(AdvancedSettings::class.java, AdvancedSettings(true))
                Toast.makeText(ktx.ctx, "( ͡° ͜ʖ ͡°)", Toast.LENGTH_LONG).show()
            }),
            //ReportVB(ktx, onTap = defaultOnTap),
            BackgroundAnimationVB(ktx, onTap = defaultOnTap),
            KeepAliveVB(ktx, onTap = defaultOnTap)
    )

    if (get(AdvancedSettings::class.java).enabled) {
        items += listOf(
            WatchdogVB(ktx, onTap = defaultOnTap),
            PowersaveVB(ktx, onTap = defaultOnTap),
            StorageLocationVB(ktx, onTap = defaultOnTap)
        )
    }

    return MenuItemsVB(ktx,
            items = items,
            name = R.string.panel_section_app_settings.res()
    )
}
