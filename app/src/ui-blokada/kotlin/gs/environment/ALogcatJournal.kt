/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.environment

import android.util.Log

class ALogcatJournal(private val tag: String) : Journal {

    override fun event(vararg events: Any) {
        Log.i(tag, "event: ${events.joinToString(separator = ";")}")
    }

    override fun log(vararg errors: Any) {
        errors.forEach { when(it) {
            is Throwable -> Log.e(tag, "------", it)
            else -> Log.v(tag, it.toString())
        }}
    }

    override fun setUserId(id: String) {
        Log.v(tag, "setUserId: $id")
    }

    override fun setUserProperty(key: String, value: Any) {
        Log.v(tag, "setUserProperty: $key=$value")
    }

}
