/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.environment

import android.app.DownloadManager
import android.app.NotificationManager
import android.content.Context
import android.os.PowerManager
import android.view.View
import com.github.salomonbrys.kodein.*
import gs.property.*
import nl.komponents.kovenant.android.androidUiDispatcher
import nl.komponents.kovenant.ui.KovenantUi

fun newGscoreModule(ctx: Context): Kodein.Module {
    return Kodein.Module {
        bind<Context>() with singleton { ctx }
        bind<Journal>() with singleton { ALogcatJournal("gscore") }
        bind<Time>() with singleton { SystemTime() }

        bind<Worker>() with multiton { it: String ->
            newSingleThreadedWorker(j = instance(), prefix = it)
        }
        bind<Worker>(2) with multiton { it: String ->
            newConcurrentWorker(j = instance(), prefix = it, tasks = 1)
        }
        bind<Worker>(3) with multiton { it: String ->
            newConcurrentWorker(j = instance(), prefix = it, tasks = 1)
        }
        bind<Worker>(10) with multiton { it: String ->
            newConcurrentWorker(j = instance(), prefix = it, tasks = 1)
        }

        bind<Serialiser>() with multiton { it: String ->
            SharedPreferencesWrapper(ctx.getSharedPreferences(it, 0))
        }

        bind<Version>() with singleton {
            VersionImpl(kctx = with("gscore").instance(2), xx = lazy)
        }
        bind<Repo>() with singleton {
            RepoImpl(kctx = with("gscore").instance(2), xx = lazy)
        }
        bind<I18n>() with singleton {
            I18nImpl(xx = lazy, kctx = with("gscore").instance(2))
        }
        bind<I18nPersistence>() with multiton { it: LanguageTag ->
            I18nPersistence(xx = lazy, locale = it)
        }

        bind<LazyProvider<View>>() with multiton { it: String -> LazyProvider<View>() }

        bind<DownloadManager>() with singleton {
            ctx.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        }
        bind<NotificationManager>() with singleton {
            ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        bind<PowerManager>() with singleton {
            ctx.getSystemService(Context.POWER_SERVICE) as PowerManager
        }

        KovenantUi.uiContext {
            dispatcher = androidUiDispatcher()
        }
    }
}
