/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.property

import gs.environment.Environment
import gs.environment.Worker
import org.blokada.BuildConfig

abstract class Version {
    abstract val appName: IProperty<String>
    abstract val name: IProperty<String>
    abstract val previousCode: IProperty<Int>
    abstract val nameCore: IProperty<String>
    abstract val obsolete: IProperty<Boolean>
}

class VersionImpl(
        kctx: Worker,
        xx: Environment
) : Version() {

    override val appName = newProperty(kctx, { "gs" })
    override val name = newProperty(kctx, { "0.0" })
    override val previousCode = newPersistedProperty(kctx, BasicPersistence(xx, "previous_code"), { 0 })
    override val nameCore = newProperty(kctx, { BuildConfig.VERSION_NAME })
    override val obsolete = newProperty(kctx, { false })
}
