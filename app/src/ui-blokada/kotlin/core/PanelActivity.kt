/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import ads.MoPubInterstitial
import ads.MoPubSdk
import android.app.Activity
import android.app.UiModeManager
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import com.github.salomonbrys.kodein.instance
import core.bits.REFRESH_HOME
import gs.environment.ComponentProvider
import gs.obsolete.Sync
import gs.presentation.ViewBinderHolder
import gs.property.Repo
import kotlinx.coroutines.runBlocking
import org.blokada.R
import tunnel.askTunnelPermission
import tunnel.tunnelPermissionResult
import update.UpdateCoordinator
import java.lang.ref.WeakReference

class PanelActivity : AppCompatActivity(), LifecycleObserver {

    private val ktx = ktx("PanelActivity")
    private val dashboardView by lazy { findViewById<DashboardView>(R.id.DashboardView) }
    private val tunnelManager by lazy { ktx.di().instance<tunnel.TunnelMain>() }
    private val filters by lazy { ktx.di().instance<Filters>() }
    private val activityContext by lazy { ktx.di().instance<ComponentProvider<Activity>>() }
    private val viewBinderHolder by lazy { ktx.di().instance<ViewBinderHolder>() }
    private var moPubSdk: MoPubSdk? = null
    private var moPubInterstitial: MoPubInterstitial? = null

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard)
        setActiveContext(activity = true)
//        setFullScreenWindowLayoutInDisplayCutout(window)
        activityRegister.register(this)
        dashboardView.onSectionClosed = {
            filters.changed %= true
        }
        activityContext.set(this)
//        getNotch()
//        if (hasSoftKeys(getSystemService(Context.WINDOW_SERVICE) as WindowManager))
//            dashboardView.navigationBarPx = resources.getDimensionPixelSize(R.dimen.dashboard_navigation_inset)
        handleUpdate()
        moPubSdk = MoPubSdk.getInstance(this)
        moPubSdk!!.setMoPubInitListener { loadInterstitial() }
    }

    private fun loadInterstitial() {
        moPubInterstitial = MoPubInterstitial(this)
        subscribeToInterstitialReadyResultChanges()
        moPubInterstitial!!.load()
    }

    private fun subscribeToInterstitialReadyResultChanges() {
        moPubInterstitial!!.isInterstitialReady.observe(this, object : Observer<Boolean> {
            override fun onChanged(aBoolean: Boolean) {
                if (aBoolean) {
                    Log.i("PanelActivity", "The moPubInterstitial is ready.")
                    moPubInterstitial!!.interstitialAd.show()
                } else {
                    Log.w("PanelActivity", "The moPubInterstitial wasn't loaded yet.")
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        modalManager.closeModal()
        if (shouldRefreshAccount) {
            shouldRefreshAccount = false
            v("check account after coming back to SubscriptionActivity")
            entrypoint.onAccountChanged()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        emit(REFRESH_HOME)
        handleUpdate()
    }

    override fun onBackPressed() {
        if (!dashboardView.handleBackPressed()) super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        viewBinderHolder.attach()
    }

    override fun onStop() {
        super.onStop()
        viewBinderHolder.detach()
    }

    override fun onDestroy() {
        super.onDestroy()
        unsetActiveContext()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        tunnelPermissionResult(Kontext.new("permission:vpn:result"), resultCode)
    }

    private fun handleUpdate() {
        if (intent.getBooleanExtra("update", false)) {
            Toast.makeText(this, R.string.update_starting, Toast.LENGTH_LONG).show()
            val repo: Repo = ktx.di().instance()
            val updateCoordinator: UpdateCoordinator = ktx.di().instance()
            updateCoordinator.start(repo.content().downloadLinks)
        }
    }

    fun trai() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // Set layout full screen
        if (Build.VERSION.SDK_INT >= 28) {
            val lp = window.attributes
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
            window.attributes = lp
        }
    }

    @RequiresApi(28)
    private fun getNotch() {
        try {
            val displayCutout = window.decorView.rootWindowInsets.displayCutout
            dashboardView.notchPx = displayCutout!!.safeInsetTop
        } catch (e: Throwable) {
            if (!isAndroidTV())
                dashboardView.notchPx = resources.getDimensionPixelSize(R.dimen.dashboard_notch_inset)
        }
    }

    private fun isAndroidTV(): Boolean {
        val uiModeManager = getSystemService(UI_MODE_SERVICE) as UiModeManager
        return uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION
    }

    fun hasNavBar(resources: Resources): Boolean {
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }

    fun hasSoftKeys(windowManager: WindowManager): Boolean {
        val d = windowManager.defaultDisplay

        val realDisplayMetrics = DisplayMetrics()
        d.getRealMetrics(realDisplayMetrics)

        val realHeight = realDisplayMetrics.heightPixels
        val realWidth = realDisplayMetrics.widthPixels

        val displayMetrics = DisplayMetrics()
        d.getMetrics(displayMetrics)

        val displayHeight = displayMetrics.heightPixels
        val displayWidth = displayMetrics.widthPixels

        return realWidth - displayWidth > 0 || realHeight - displayHeight > 0
    }
}

val modalManager = ModalManager()
val activityRegister = ActiveActivityRegister()

class ActiveActivityRegister {

    private var activity = Sync(WeakReference(null as Activity?))

    fun register(activity: Activity) {
        this.activity = Sync(WeakReference(activity))
    }

    fun get() = activity.get().get()

    fun askPermissions() {
        val act = activity.get().get() ?: throw Exception("starting MainActivity")
        val deferred = askTunnelPermission(Kontext.new("static perm ask"), act)
        runBlocking {
            val response = deferred.await()
            if (!response) { throw Exception("could not get tunnel permissions") }
        }
    }

    fun getParentView(): View? {
        return activity.get().get()?.findViewById(R.id.root)
    }
}
