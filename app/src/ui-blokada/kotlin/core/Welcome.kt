/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.content.Context
import com.github.salomonbrys.kodein.*
import gs.environment.Environment
import gs.environment.Worker
import gs.property.*

abstract class Welcome {
    abstract val introSeen: IProperty<Boolean>
    abstract val guideSeen: IProperty<Boolean>
    abstract val patronShow: IProperty<Boolean>
    abstract val patronSeen: IProperty<Boolean>
    abstract val ctaSeenCounter: IProperty<Int>
    abstract val advanced: IProperty<Boolean>
}

class WelcomeImpl (
        w: Worker,
        xx: Environment,
        val i18n: I18n = xx().instance()
) : Welcome() {
    override val introSeen = newPersistedProperty(w, BasicPersistence(xx, "intro_seen"), { false })
    override val guideSeen = newPersistedProperty(w, BasicPersistence(xx, "guide_seen"), { false })
    override val patronShow = newProperty(w, { false })
    override val patronSeen = newPersistedProperty(w, BasicPersistence(xx, "optional_seen"), { false })
    override val ctaSeenCounter = newPersistedProperty(w, BasicPersistence(xx, "cta_seen"), { 3 })
    override val advanced = newPersistedProperty(w, BasicPersistence(xx, "advanced"), { false })

    init {
        i18n.locale.doWhenSet().then {
            patronShow %= true
        }

    }
}

fun newWelcomeModule(ctx: Context): Kodein.Module {
    return Kodein.Module {
        bind<Welcome>() with singleton {
            WelcomeImpl(w = with("gscore").instance(2), xx = lazy)
        }
    }
}

