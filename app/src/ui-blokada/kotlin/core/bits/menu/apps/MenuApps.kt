/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu.apps

import core.AndroidKontext
import core.LabelVB
import core.bits.menu.MenuItemVB
import core.bits.menu.MenuItemsVB
import core.res
import gs.presentation.NamedViewBinder
import org.blokada.R

fun createAppsMenuItem(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemVB(ktx,
            label = R.string.panel_section_apps.res(),
            icon = R.drawable.ic_apps.res(),
            opens = createMenuApps(ktx)
    )
}

private fun createMenuApps(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemsVB(ktx,
            items = listOf(
                    LabelVB(ktx, label = R.string.menu_apps_intro.res()),
                    createInstalledAppsMenuItem(ktx),
                    LabelVB(ktx, label = R.string.menu_apps_system_label.res()),
                    createAllAppsMenuItem(ktx)
            ),
            name = R.string.panel_section_apps.res()
    )
}

private fun createInstalledAppsMenuItem(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemVB(ktx,
            label = R.string.panel_section_apps_all.res(),
            icon = R.drawable.ic_apps.res(),
            opens = AllAppsDashboardSectionVB(ktx.ctx, system = false))
}

private fun createAllAppsMenuItem(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemVB(ktx,
            label = R.string.panel_section_apps_system.res(),
            icon = R.drawable.ic_apps.res(),
            opens = AllAppsDashboardSectionVB(ktx.ctx, system = true))
}
