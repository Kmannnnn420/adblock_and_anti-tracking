/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu.dns

import android.content.Context
import com.github.salomonbrys.kodein.instance
import core.*
import core.bits.*
import core.bits.menu.adblocking.SlotMutex
import gs.presentation.ListViewBinder
import gs.presentation.NamedViewBinder
import gs.property.IWhen
import org.blokada.R
import tunnel.DnsAnswerTypeVB

class DnsDashboardSection(
        val ctx: Context,
        override val name: Resource = R.string.panel_section_advanced_dns.res()
) : ListViewBinder(), NamedViewBinder {

    private val ktx = ctx.ktx("DnsDashboard")
    private val filters by lazy { ktx.di().instance<Filters>() }
    private val dns by lazy { ktx.di().instance<Dns>() }

    private val slotMutex = SlotMutex()

    private var get: IWhen? = null

    override fun attach(view: VBListView) {
        view.enableAlternativeMode()
        filters.apps.refresh()
        get = dns.choices.doOnUiWhenSet().then {
            val default = dns.choices().firstOrNull()
            val active = dns.choices().filter { it.active }
            val inactive = dns.choices().filter { !it.active }

            val defaultList = if (default != null) listOf(default) else emptyList()

            (defaultList + active.minus(defaultList) + inactive.minus(defaultList)).map {
                DnsChoiceVB(it, ktx, onTap = slotMutex.openOneAtATime)
            }.apply {
                view.set(this)
                view.add(LabelVB(ktx, label = R.string.menu_dns_intro.res()), 0)
                view.add(MenuActiveDnsVB(ktx), 1)
                view.add(AddDnsVB(ktx), 2)
                view.add(LabelVB(ktx, label = R.string.menu_dns_recommended.res()), 3)
                view.add(LabelVB(ktx, label = R.string.menu_manage.res()))
                view.add(DnsListControlVB(ktx, onTap = defaultOnTap))
                view.add(DnsFallbackVB(ktx, onTap = defaultOnTap))
                view.add(DnsAnswerTypeVB(ktx, onTap = defaultOnTap))
            }
        }
    }

    override fun detach(view: VBListView) {
        slotMutex.detach()
        dns.choices.cancel(get)
    }

}
