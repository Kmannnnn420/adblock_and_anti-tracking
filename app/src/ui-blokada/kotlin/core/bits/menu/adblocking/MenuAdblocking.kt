/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu.adblocking

import core.AndroidKontext
import core.LabelVB
import core.bits.Adblocking2VB
import core.bits.menu.MenuItemVB
import core.bits.menu.MenuItemsVB
import core.res
import gs.presentation.NamedViewBinder
import org.blokada.R

private fun createMenuAdblocking(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemsVB(ktx,
            items = listOf(
                Adblocking2VB(ktx),
                LabelVB(ktx, label = R.string.menu_ads_log_label.res()),
                createHostsLogMenuItem(ktx),
                LabelVB(ktx, label = R.string.menu_ads_lists_label.res()),
                createHostsListMenuItem(ktx),
                LabelVB(ktx, label = R.string.menu_ads_rules_label.res()),
                createWhitelistMenuItem(ktx),
                createBlacklistMenuItem(ktx),
                createAdblockingSettingsMenuItem(ktx)
            ),
            name = R.string.panel_section_ads.res()
    )
}

fun createAdblockingMenuItem(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemVB(ktx,
            label = R.string.panel_section_ads.res(),
            icon = R.drawable.ic_blocked.res(),
            opens = createMenuAdblocking(ktx)
    )
}
