/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import androidx.appcompat.app.AppCompatActivity

/**
 * An invisible activity to get the app out of powersave limitations.
 *
 * When some devices, like Huawei, get into some kind of powersave mode, they start to block network
 * access causing the tunnel's send attempts to fail. Bringing up activity raises the app out of
 * those limitations. This activity is transparent and finishes immediately, to not be visible to
 * user. I couldn't find a nicer way to do this.
 */
class PowersaveActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        finish()
    }
}
