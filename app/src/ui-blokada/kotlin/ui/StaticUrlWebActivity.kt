/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ui

import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import core.WebViewActivity.Companion.EXTRA_URL
import core.ktx
import gs.environment.Worker
import gs.property.newProperty
import java.net.URL


class StaticUrlWebActivity : AbstractWebActivity() {

    private val ktx = ktx("StaticUrlWebActivity")
    private val w: Worker by lazy { ktx.di().with("gscore").instance<Worker>() }

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        targetUrl = newProperty(w, { URL(intent.getStringExtra(EXTRA_URL)) })
        super.onCreate(savedInstanceState)
    }

}
