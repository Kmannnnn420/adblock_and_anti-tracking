/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ui.bits.menu.vpn

import android.os.Handler
import blocka.BlockaRestApi
import blocka.BlockaRestModel
import blocka.CurrentAccount
import blocka.MAX_RETRIES
import com.github.salomonbrys.kodein.instance
import core.*
import core.bits.menu.adblocking.SlotMutex
import gs.presentation.ListViewBinder
import gs.presentation.NamedViewBinder
import gs.presentation.ViewBinder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.blokada.R
import retrofit2.Call
import retrofit2.Response

class LeasesDashboardSectionVB(
        val ktx: AndroidKontext,
        val api: BlockaRestApi = ktx.di().instance(),
        override val name: Resource = R.string.menu_vpn_leases.res()
) : ListViewBinder(), NamedViewBinder {

    private val slotMutex = SlotMutex()

    private var items = listOf<ViewBinder>(
            LabelVB(ktx, label = R.string.slot_leases_info.res())
    )

    private val request = Handler {
        GlobalScope.async { populate() }
        true
    }

    override fun attach(view: VBListView) {
        view.enableAlternativeMode()
        view.set(items)
        request.sendEmptyMessage(0)
    }

    override fun detach(view: VBListView) {
        slotMutex.detach()
        request.removeMessages(0)
    }

    private fun populate(retry: Int = 0) {
        val currentAccount = get(CurrentAccount::class.java)
        api.getLeases(currentAccount.id).enqueue(object : retrofit2.Callback<BlockaRestModel.Leases> {
            override fun onFailure(call: Call<BlockaRestModel.Leases>?, t: Throwable?) {
                ktx.e("leases api call error", t ?: "null")
                if (retry < MAX_RETRIES) populate(retry + 1)
                else request.sendEmptyMessageDelayed(0, 5 * 1000)
            }

            override fun onResponse(call: Call<BlockaRestModel.Leases>?, response: Response<BlockaRestModel.Leases>?) {
                response?.run {
                    when (code()) {
                        200 -> {
                            body()?.run {
                                val g = leases.map {
                                    LeaseVB(ktx, it, onTap = slotMutex.openOneAtATime,
                                            onRemoved = {
                                                items = items - it
                                                view?.set(items)
                                                request.sendEmptyMessageDelayed(0, 2000)
                                            })
                                }
                                items = listOf(
                                    LabelVB(ktx, label = R.string.slot_leases_info.res())
                                ) + g
                                view?.set(items)
                            }
                        }
                        else -> {
                            ktx.e("leases api call response ${code()}")
                            if (retry < MAX_RETRIES) populate(retry + 1)
                            else request.sendEmptyMessageDelayed(0, 30 * 1000)
                            Unit
                        }
                    }
                }
            }
        })
    }
}
