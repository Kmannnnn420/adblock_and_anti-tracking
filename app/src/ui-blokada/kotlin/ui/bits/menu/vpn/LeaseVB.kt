/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ui.bits.menu.vpn

import android.os.Build
import blocka.CurrentAccount
import blocka.blockaVpnMain
import com.github.salomonbrys.kodein.instance
import core.*
import gs.property.I18n
import org.blokada.R
import blocka.BlockaRestModel

class LeaseVB(
        val ktx: AndroidKontext,
        private val lease: BlockaRestModel.LeaseInfo,
        val i18n: I18n = ktx.di().instance(),
        val onRemoved: (LeaseVB) -> Unit = {},
        onTap: (SlotView) -> Unit
) : SlotVB(onTap) {

    private fun update() {
        val cfg = get(CurrentAccount::class.java)
        val currentDevice = lease.publicKey == cfg.publicKey
        view?.apply {
            content = Slot.Content(
                    label = if (currentDevice)
                        i18n.getString(R.string.slot_lease_name_current, "%s-%s".format(
                                Build.MANUFACTURER, Build.DEVICE
                        ))
                        else lease.niceName(),
                    icon = ktx.ctx.getDrawable(R.drawable.ic_device),
                    description = if (currentDevice) {
                        i18n.getString(R.string.slot_lease_description_current, lease.publicKey)
                    } else {
                        i18n.getString(R.string.slot_lease_description, lease.publicKey)
                    },
                    action1 = if (currentDevice) null else ACTION_REMOVE
            )

            onRemove = {
                blockaVpnMain.deleteLease(BlockaRestModel.LeaseRequest(
                        accountId = cfg.id,
                        publicKey = lease.publicKey,
                        gatewayId = lease.gatewayId,
                        alias = ""
                ))
                onRemoved(this@LeaseVB)
            }
        }
    }

    override fun attach(view: SlotView) {
        view.enableAlternativeBackground()
        view.type = Slot.Type.INFO
        on(CurrentAccount::class.java, this::update)
        update()
    }

    override fun detach(view: SlotView) {
        cancel(CurrentAccount::class.java, this::update)
    }
}
