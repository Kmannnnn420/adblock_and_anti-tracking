/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import com.github.salomonbrys.kodein.instance
import core.Tunnel
import core.TunnelState
import gs.environment.Environment
import gs.environment.Journal
import gs.property.IWhen
import java.net.URL

/**
 * It makes sure AdBlock is inactive during update download.
 */
class UpdateCoordinator(
        private val xx: Environment,
        private val downloader: AUpdateDownloader,
        private val s: Tunnel = xx().instance(),
        private val j: Journal = xx().instance()
) {

    private var w: IWhen? = null
    private var downloading = false

    fun start(urls: List<URL>) {
        if (downloading) return
        if (s.tunnelState(TunnelState.INACTIVE)) {
            download(urls)
        }
        else {
//            j.log("UpdateCoordinator: deactivate tunnel: ${s.tunnelState()}")
//            s.tunnelState.cancel(w)
//            w = s.tunnelState.doOnUiWhenChanged().then {
//                if (s.tunnelState(TunnelState.INACTIVE)) {
//                    if (!downloading) {
//                        j.log("UpdateCoordinator: tunnel deactivated")
//                        s.tunnelState.cancel(w)
//                        w = null
                        download(urls)
//                    }
//                }
//            }
//
//            s.updating %= true
//            s.restart %= true
//            s.active %= false
        }
    }

    private fun download(urls: List<URL>) {
        j.log("UpdateCoordinator: start download")
        downloading = true
        downloader.downloadUpdate(urls, { uri ->
            j.log("UpdateCoordinator: downloaded: url $uri")
            if (uri != null) downloader.openInstall(uri)
            s.updating %= false
            downloading = false
        })
    }

}

