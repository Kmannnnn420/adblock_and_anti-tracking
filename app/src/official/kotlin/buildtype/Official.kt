/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package buildtype

import android.app.Application
import android.content.Context
import android.text.format.DateUtils
import android.util.Log
import com.github.salomonbrys.kodein.*
import core.Tunnel
import gs.environment.*
import gs.property.BasicPersistence
import gs.property.Device
import gs.property.IProperty
import gs.property.newPersistedProperty
import org.blokada.R

fun newBuildTypeModule(ctx: Context): Kodein.Module {
    return Kodein.Module {
        bind<Journal>(overrides = true) with singleton {
            ALogcatJournal("gscore")
        }
        bind<Events>() with singleton {
            EventsImpl(kctx = with("gscore").instance(), xx = lazy)
        }
        onReady {
            val e: Events = instance()
            val d: Device = instance()

            // I assume this will happen at least once a day
            d.screenOn.doWhenChanged().then {
                if (d.reports()) {
                    e.lastDailyMillis.refresh()
                    e.lastActiveMillis.refresh()
                }
            }

            // This will happen when loading the app to memory
            if (d.reports()) {
                e.lastDailyMillis.refresh()
                e.lastActiveMillis.refresh()
            }
        }
    }
}

abstract class Events {
    abstract val lastDailyMillis: IProperty<Long>
    abstract val lastActiveMillis: IProperty<Long>
}

class EventsImpl(
        private val kctx: Worker,
        private val xx: Environment,
        private val time: Time = xx().instance(),
        private val j: Journal = xx().instance(),
        private val t: Tunnel = xx().instance()
) : Events() {
    override val lastDailyMillis = newPersistedProperty(kctx, BasicPersistence(xx, "daily"), { 0L },
            refresh = {
                j.event("daily")
                time.now()
            },
            shouldRefresh = { !DateUtils.isToday(it) })

    override val lastActiveMillis = newPersistedProperty(kctx, BasicPersistence(xx, "daily-active"), { 0L },
            refresh = {
                if (t.active()) {
                    j.event("daily-active")
                    time.now()
                } else it
            },
            shouldRefresh = { !DateUtils.isToday(it) })
}

