/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import blocka.CurrentLease
import java.io.FileDescriptor
import java.net.InetSocketAddress


data class TunnelConfig(
        val tunnelEnabled: Boolean = false,
        val adblocking: Boolean = true,
        val wifiOnly: Boolean = true,
        val firstLoad: Boolean = true,
        val powersave: Boolean = false,
        val dnsFallback: Boolean = true,
        val report: Boolean = false, // TODO: gone from here
        val wildcards: Boolean = false,
        var filtersUrl: String? = null,
        val cacheTTL: Long = 86400
)

data class CurrentTunnel(
        val dnsServers: List<InetSocketAddress> = emptyList(),
        val adblocking: Boolean = false,
        val blockaVpn: Boolean = false,
        val lease: CurrentLease? = null,
        val userBoringtunPrivateKey: String? = null
) {
    override fun toString(): String {
        return "CurrentTunnel(dnsServers=$dnsServers, adblocking=$adblocking, blockaVpn=$blockaVpn, lease=$lease)"
    }
}

data class TunnelDescriptor(
        val tunnel: Tunnel?,
        val thread: Thread?,
        val fd: FileDescriptor,
        val binder: ServiceBinder
)
