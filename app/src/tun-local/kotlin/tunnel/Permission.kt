/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import android.app.Activity
import android.net.VpnService
import core.AndroidKontext
import core.Kontext
import kotlinx.coroutines.CompletableDeferred

private var deferred = CompletableDeferred<Boolean>()

fun askTunnelPermission(ktx: Kontext, act: Activity) = {
    ktx.v("asking for tunnel permissions")
    deferred.completeExceptionally(Exception("new permission request"))
    deferred = CompletableDeferred()
    val intent = VpnService.prepare(act)
    when (intent) {
        null -> deferred.complete(true)
        else -> act.startActivityForResult(intent, 0)
    }
    deferred
}()

fun tunnelPermissionResult(ktx: Kontext, code: Int) = {
    ktx.v("received tunnel permissions response", code)
    when {
        deferred.isCompleted -> Unit
        code == -1 -> deferred.complete(true)
        else -> deferred.completeExceptionally(Exception("permission result: $code"))
    }
}()

fun checkTunnelPermissions(ktx: AndroidKontext) {
    if (VpnService.prepare(ktx.ctx) != null) {
        throw Exception("no tunnel permissions")
    }
}
