/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package blocka

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.github.salomonbrys.kodein.instance
import core.*
import gs.property.Device
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.*

fun scheduleAccountChecks() = {
    scheduleDailyRecheck()
    scheduleAlarmRecheck()
}()

private fun scheduleDailyRecheck() = {
    val ctx = getActiveContext()!!
    val ktx = ctx.ktx("daily-account-recheck")
    val d: Device = ktx.di().instance()
    d.screenOn.doOnUiWhenChanged().then {
        if (d.screenOn()) GlobalScope.async {
            val account = get(CurrentAccount::class.java)
            if (account.lastAccountCheck + 86400 * 1000 < System.currentTimeMillis()) {
                v("daily account recheck")
                entrypoint.onAccountChanged()
            }
        }
    }
}()

class RenewLicenseReceiver : BroadcastReceiver() {
    override fun onReceive(ctx: Context, p1: Intent) {
        GlobalScope.async {
            v("recheck account / lease task executing")
            entrypoint.onAccountChanged()
        }
    }
}

private fun scheduleAlarmRecheck() {
    val ctx = getActiveContext()!!
    val alarm: AlarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val operation = Intent(ctx, RenewLicenseReceiver::class.java).let { intent ->
        PendingIntent.getBroadcast(ctx, 0, intent, 0)
    }

    val account = get(CurrentAccount::class.java)
    val lease = get(CurrentLease::class.java)
    val accountTime = account.activeUntil
    val leaseTime = lease.leaseActiveUntil
    val sooner = if (accountTime.before(leaseTime)) accountTime else leaseTime
    if (sooner.before(Date())) {
        entrypoint.onAccountChanged()
    } else {
        alarm.set(AlarmManager.RTC, sooner.time, operation)
        v("scheduled account / lease recheck for $sooner")
    }

}

private fun Date.minus(minutes: Int) = Date(this.time - minutes * 60 * 1000)
