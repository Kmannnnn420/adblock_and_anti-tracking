/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits.menu.advanced

import core.AndroidKontext
import core.LabelVB
import core.bits.*
import core.bits.menu.MenuItemsVB
import core.defaultOnTap
import core.res
import gs.presentation.NamedViewBinder
import org.blokada.R

fun createMenuAdvanced(ktx: AndroidKontext): NamedViewBinder {
    return MenuItemsVB(ktx,
            items = listOf(
                    LabelVB(ktx, label = R.string.label_basic.res()),
                    StartOnBootVB(ktx, onTap = defaultOnTap),
                    StorageLocationVB(ktx, onTap = defaultOnTap),
                    LabelVB(ktx, label = R.string.label_advanced.res()),
                    KeepAliveVB(ktx, onTap = defaultOnTap),
                    WatchdogVB(ktx, onTap = defaultOnTap),
                    PowersaveVB(ktx, onTap = defaultOnTap) //,
                    //ReportVB(ktx, onTap = defaultOnTap)
            ),
            name = R.string.panel_section_advanced_settings.res()
    )
}
