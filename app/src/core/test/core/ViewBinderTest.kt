/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import gs.presentation.ViewTypeGenerator
import org.junit.Test

class ViewBinderTest {
    @Test fun viewTypeGenerator_basics() {
        val viewBinder1 = "vb1"
        val viewBinder2 = "vb2"
        val payload1 = "1"
        val payload2 = "2"
        val payload3 = 1

        val type1 = ViewTypeGenerator.get(viewBinder1)
        val type2 = ViewTypeGenerator.get(viewBinder2)
        val type11 = ViewTypeGenerator.get(viewBinder1, payload1)
        val type11_2 = ViewTypeGenerator.get(viewBinder1, payload1)
        val type12 = ViewTypeGenerator.get(viewBinder1, payload2)
        val type13 = ViewTypeGenerator.get(viewBinder1, payload3)
        val type21 = ViewTypeGenerator.get(viewBinder2, payload1)

        assert(type1 != type2)
        assert(type1 != type11)
        assert(type2 != type11)

        assert(type11 != type12)
        assert(type11 != type13)
        assert(type21 != type11)

        assert(type11 == type11_2)
    }
}
