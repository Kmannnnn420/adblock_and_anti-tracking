/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import kotlinx.coroutines.Unconfined
import org.junit.Assert
import org.junit.Test

class EmitTest {
    @Test fun emit_basics() {
        val hello = "hello".newEvent()
        val anotherHello = "hello".newEvent()

        var count = 0
        val callback = { ++count; Unit }

        val emit = CommonEmit({ Kontext.forTest() })

        emit.on(hello, callback)
        Assert.assertEquals(0, count)

        emit.emit(anotherHello)
        Assert.assertEquals(0, count)

        emit.emit(hello)
        Assert.assertEquals(1, count)

        emit.emit(hello)
        Assert.assertEquals(2, count)

        emit.cancel(hello, callback)
        emit.emit(hello)
        Assert.assertEquals(2, count)
    }

    @Test fun emit_noDeepCopy() {
        val data = mutableListOf("a", "b")

        val emit = CommonEmit({ Kontext.forTest() })

        var received = emptyList<String>()
        val callback = { it: List<String> -> received = it }
        val event = "event".newEventOf<List<String>>()

        emit.on(event, callback)
        emit.emit(event, data)
        Assert.assertEquals(data, received)

        data.add("c")
        Assert.assertEquals("c", received[2])
    }

    @Test fun emit_sendsMostRecentEventOnSubscribe() {
        val emit = CommonEmit({ Kontext.forTest() })

        var received = 0
        val callback = { it: Int -> received = it }
        val event = "event".newEventOf<Int>()

        emit.emit(event, 1)

        Assert.assertEquals(0, received)
        emit.on(event, callback)
        Assert.assertEquals(1, received)
    }

    @Test fun emit_logsException() {
        var logged = 0
        val emit = CommonEmit({ Kontext.forTest(coroutineContext = Unconfined + newEmitExceptionLogger(
                Kontext.forTest("emit:exception",
                        log = object : Log {
                            override fun e(vararg msgs: Any) { logged++ }
                            override fun w(vararg msgs: Any) {}
                            override fun v(vararg msgs: Any) {}
                        })
        )) })

        val callback = { throw Exception("crash") }

        Assert.assertEquals(0, logged)
        val event = "event".newEvent()
        emit.on(event, callback)
        emit.emit(event)
        Assert.assertEquals(1, logged)
    }
}
