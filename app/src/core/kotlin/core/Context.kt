/*
Copyright 2020 blokada of https://blokada.org represented by 
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.content.Context
import java.lang.ref.WeakReference

private var appContext: WeakReference<Context?> = WeakReference(null)
private var activityContext: WeakReference<Context?> = WeakReference(null)

@Synchronized fun getActiveContext(activity: Boolean = false): Context? {
    return when {
        activityContext.get() != null -> activityContext.get()
        !activity && appContext.get() != null -> appContext.get()
        else -> {
            throw Exception("No context set (activity: $activity)")
        }
    }
}

@Synchronized fun Context.setActiveContext(activity: Boolean = false) {
    if (activity) activityContext = WeakReference(this@setActiveContext)
    else appContext = WeakReference(this@setActiveContext)
}

@Synchronized fun unsetActiveContext() {
    activityContext = WeakReference(null)
}
