/*
Copyright 2020 blokada of https://blokada.org represented by 
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package adblocker

import android.app.Service
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.os.IBinder
import kotlinx.coroutines.runBlocking
import notification.KeepAliveNotification
import notification.notificationMain


class ForegroundStartService: Service(){

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification = KeepAliveNotification()
            val n = runBlocking { notificationMain.getNotification(notification).await() }
            startForeground(notification.id, n)
        }

        val wm: AppWidgetManager = AppWidgetManager.getInstance(this)
        val ids = wm.getAppWidgetIds(ComponentName(this, ActiveWidgetProvider::class.java))
        if((ids != null) and (ids.isNotEmpty())){
            val serviceIntent = Intent(this.applicationContext,
                    UpdateWidgetService::class.java)
            this.startService(serviceIntent)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(STOP_FOREGROUND_REMOVE)
        }

        stopSelf()
        return START_NOT_STICKY
    }

}
