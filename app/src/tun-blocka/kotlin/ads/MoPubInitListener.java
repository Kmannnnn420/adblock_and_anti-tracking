package ads;

/**
 * Created by Sönke Gissel on 23.09.2020.
 */
public interface MoPubInitListener {
    void onInitialized();
}
