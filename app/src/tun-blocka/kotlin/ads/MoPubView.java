package ads;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.mopub.mobileads.MoPubErrorCode;

/**
 * Created by Sönke Gissel on 27.06.2019.
 */
class MoPubView extends Ad {

    private com.mopub.mobileads.MoPubView moPubView;
    private Context mContext;

    MoPubView(Context context) {
        super(context);
        this.mContext = context;
        moPubView = new com.mopub.mobileads.MoPubView(mContext);

        moPubView.setVisibility(View.GONE);
        moPubView.setAdUnitId(getAdUnitId(this));
        moPubView.setAdSize(getAdSize());

        BannerAdListener listener = new BannerAdListener();
        moPubView.setBannerAdListener(listener);
    }

    // GETTER
    com.mopub.mobileads.MoPubView getMoPubView() {
        return moPubView;
    }

    @Override
    public void destroy() {
        moPubView.destroy();
    }

    @Override
    public void load() {
        moPubView.loadAd();
        Log.d(this.getClass().getSimpleName(), "AdSize: "+moPubView.getAdSize().name());
    }

    private class BannerAdListener implements com.mopub.mobileads.MoPubView.BannerAdListener {
        @Override
        public void onBannerLoaded(com.mopub.mobileads.MoPubView banner) {
            Log.d(this.getClass().getSimpleName(), "onBannerLoaded.");
            moPubView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onBannerFailed(com.mopub.mobileads.MoPubView banner, MoPubErrorCode errorCode) {
            Log.d(this.getClass().getSimpleName(), "onBannerFailed. With error "+errorCode.toString());
            moPubView.setVisibility(View.GONE);
        }

        @Override
        public void onBannerClicked(com.mopub.mobileads.MoPubView banner) {
            Log.d(this.getClass().getSimpleName(), "onBannerClicked.");
        }

        @Override
        public void onBannerExpanded(com.mopub.mobileads.MoPubView banner) {
            Log.d(this.getClass().getSimpleName(), "onBannerExpanded.");
        }

        @Override
        public void onBannerCollapsed(com.mopub.mobileads.MoPubView banner) {
            Log.d(this.getClass().getSimpleName(), "onBannerCollapsed.");
        }
    }

    private com.mopub.mobileads.MoPubView.MoPubAdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        int orientation = Resources.getSystem().getConfiguration().orientation;
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float widthPixels;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            widthPixels = metrics.widthPixels;
        } else {
            widthPixels = metrics.heightPixels;
        }
        float density = metrics.density;
        int adWidth = (int) (widthPixels / density);
        Log.d(this.getClass().getSimpleName(), "widthPixels "+widthPixels+" density "+density+" adWidth "+adWidth);
        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        if(adWidth < 480) {
            return com.mopub.mobileads.MoPubView.MoPubAdSize.HEIGHT_50;
        } else {
            return com.mopub.mobileads.MoPubView.MoPubAdSize.HEIGHT_90;
        }
    }
}
