package ads;

import android.util.Log;
import android.widget.FrameLayout;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import org.jetbrains.annotations.NotNull;

import kotlin.jvm.internal.Intrinsics;

/**
 * Created by Sönke Gissel on 27.06.2019.
 */
public class MoPubViewLifecycleObserver implements LifecycleObserver {
    private final MoPubView moPubView;
    private final FrameLayout mFrameLayout;

    MoPubViewLifecycleObserver(@NotNull MoPubView moPubView, FrameLayout frameLayout) {
        Intrinsics.checkParameterIsNotNull(moPubView, "moPubView");
        this.moPubView = moPubView;
        this.mFrameLayout = frameLayout;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public final void onResume() {
        Log.d(this.getClass().getSimpleName(), "onResume.");
        this.mFrameLayout.addView(moPubView.getMoPubView());
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public final void onPause() {
        Log.d(this.getClass().getSimpleName(), "onPause.");
        this.mFrameLayout.removeView(moPubView.getMoPubView());
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public final void onDestroy() {
        Log.d(this.getClass().getSimpleName(), "onDestroy.");
        this.moPubView.destroy();
    }
}
